package org.task.test.api.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.task.test.core.task.NewPrintTaskSchedulerServiceImpl;
import org.task.test.core.task.TaskSchedulerServiceImpl;
import org.wso2.carbon.ntask.core.TaskInfo;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/task")
public class TaskApiImpl implements TaskApi {

    private static final Log log = LogFactory.getLog(TaskApiImpl.class);

    @POST
    @Path("/logging")
    @Override
    public Response createLoggingTask(LoggingTaskDetails update) {
        try {
            log.info("Received request to create logging task");
            startLoggingTask(update.stringToLog, update.frequencyInSeconds);
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.status(Response.Status.EXPECTATION_FAILED).build();
        }
    }

    @PUT
    @Path("/logging")
    @Override
    public Response updateLoggingTask(LoggingTaskDetails update) {
        try {
            log.info("Received request to update logging task");
            scheduleTaskService(update.stringToLog, update.frequencyInSeconds);
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.status(Response.Status.EXPECTATION_FAILED).build();
        }
    }

    @DELETE
    @Path("/logging")
    @Override
    public Response deleteLoggingTask() {
        try {
            log.info("Received request to delete task");
            TaskSchedulerServiceImpl taskScheduleService = new TaskSchedulerServiceImpl();
            taskScheduleService.stopTask();
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.status(Response.Status.EXPECTATION_FAILED).build();
        }
    }

    @Override
    @GET
    @Path("/logging")
    public Response getAllLoggingTasks() {
        try {
            log.info("Received request to get all logging tasks");
            TaskSchedulerServiceImpl taskScheduleService = new TaskSchedulerServiceImpl();
            List<TaskInfo> allTasks = taskScheduleService.getAllTasks();
            return Response.status(Response.Status.OK).entity(allTasks).build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.status(Response.Status.EXPECTATION_FAILED).build();
        }
    }

    @POST
    @Path("/print")
    @Override
    public Response createNewPrintTask(PrintTaskDetails printTaskDetails) {
        try {
            log.info("Received request to create new print task " + printTaskDetails.taskName);
            scheduleNewTask(printTaskDetails.taskName, printTaskDetails.stringToLog, printTaskDetails.frequencyInSeconds);
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.status(Response.Status.EXPECTATION_FAILED).build();
        }
    }


    @PUT
    @Path("/print")
    @Override
    public Response updatePrintTask(PrintTaskDetails update) {
        try {
            log.info("Received request to update print task");
            NewPrintTaskSchedulerServiceImpl newTaskScheduleService = new NewPrintTaskSchedulerServiceImpl();
            if (newTaskScheduleService.isTaskScheduled(update.taskName)) {
                newTaskScheduleService.updateTask(update.taskName, update.stringToLog, update.frequencyInSeconds);
            } else {
                newTaskScheduleService.startTaskInSeconds(update.taskName, update.stringToLog, update.frequencyInSeconds);
            }
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.status(Response.Status.EXPECTATION_FAILED).build();
        }
    }

    @DELETE
    @Path("/print")
    @Override
    public Response deletePrintTask(PrintTaskDetails printTaskDetails) {
        try {
            log.info("Received request to delete task " + printTaskDetails.taskName);
            deleteNewTask(printTaskDetails.taskName);
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.status(Response.Status.EXPECTATION_FAILED).build();
        }
    }

    @DELETE
    @Path("/print/all")
    @Override
    public Response deleteAllPrintTask() {
        try {
            log.info("Received request to delete all new tasks");
            NewPrintTaskSchedulerServiceImpl newTaskScheduleService = new NewPrintTaskSchedulerServiceImpl();
            newTaskScheduleService.stopAllTasks();
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.status(Response.Status.EXPECTATION_FAILED).build();
        }
    }


    @Override
    @GET
    @Path("/print")
    public Response getAllPrintTasks() {
        try {
            log.info("Received request to get all logging tasks");
            NewPrintTaskSchedulerServiceImpl newTaskScheduleService = new NewPrintTaskSchedulerServiceImpl();
            List<TaskInfo> allTasks = newTaskScheduleService.getAllTasks();
            return Response.status(Response.Status.OK).entity(allTasks).build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.status(Response.Status.EXPECTATION_FAILED).build();
        }
    }

    private void deleteNewTask(String taskName) {
        NewPrintTaskSchedulerServiceImpl newTaskSchedulerService = new NewPrintTaskSchedulerServiceImpl();
        if (newTaskSchedulerService.isTaskScheduled(taskName)) {
            newTaskSchedulerService.stopTask(taskName);
        } else {
            log.error("No such task scheduled to delete");
        }
    }

    private void scheduleNewTask(String taskName, String stringToLog, long frequencyInSeconds) {
        NewPrintTaskSchedulerServiceImpl newTaskSchedulerService = new NewPrintTaskSchedulerServiceImpl();
        if (newTaskSchedulerService.isTaskScheduled(taskName)) {
            newTaskSchedulerService.updateTask(taskName, stringToLog, frequencyInSeconds);
        } else {
            newTaskSchedulerService.startTaskInSeconds(taskName, stringToLog, frequencyInSeconds);
        }
    }

    private void scheduleTaskService(String stringToLog, long frequencyInSeconds) {
        TaskSchedulerServiceImpl taskScheduleService = new TaskSchedulerServiceImpl();
        if (taskScheduleService.isTaskScheduled()) {
            taskScheduleService.updateTask(stringToLog, frequencyInSeconds);
        } else {
            taskScheduleService.startTask(stringToLog, frequencyInSeconds);
        }
    }

    private void startLoggingTask(String stringToLog, long frequencyInSeconds) {
        TaskSchedulerServiceImpl taskScheduleService = new TaskSchedulerServiceImpl();
        taskScheduleService.startTask(stringToLog, frequencyInSeconds);
    }
}
