package org.task.test.api.service;

import io.swagger.annotations.Api;
import io.swagger.annotations.Extension;
import io.swagger.annotations.ExtensionProperty;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@SwaggerDefinition(
        info = @Info(
                version = "1.0.0",
                title = "Application Management Artifact Download Service",
                extensions = {
                        @Extension(properties = {
                                @ExtensionProperty(name = "name", value = "ApplicationManagementArtifactDownloadService"),
                                @ExtensionProperty(name = "context", value = "/api/task-mgt/v1.0/task"),
                        })
                }
        )
)
@Path("/task")
@Api(value = "ApplicationDTO Management Artifact Downloading Service")
@Produces(MediaType.APPLICATION_JSON)
public interface TaskApi {

    @POST
    @Path("/logging")
    Response createLoggingTask(LoggingTaskDetails update);

    @PUT
    @Path("/logging")
    Response updateLoggingTask(LoggingTaskDetails update);

    @DELETE
    @Path("/logging")
    Response deleteLoggingTask();


    @GET
    @Path("/logging")
    Response getAllLoggingTasks();

    @PUT
    @Path("/print")
    Response updatePrintTask(PrintTaskDetails update);

    @POST
    @Path("/print")
    Response createNewPrintTask(PrintTaskDetails printTaskDetails);

    @DELETE
    @Path("/print")
    Response deletePrintTask(PrintTaskDetails printTaskDetails);

    @DELETE
    @Path("/print/all")
    Response deleteAllPrintTask();

    @GET
    @Path("/print")
    Response getAllPrintTasks();
}
