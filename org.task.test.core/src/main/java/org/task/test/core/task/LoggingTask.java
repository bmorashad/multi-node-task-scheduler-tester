package org.task.test.core.task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.task.test.core.util.Constants;
import org.wso2.carbon.ntask.core.Task;

import java.util.Map;

public class LoggingTask implements Task {

    private static final Log log = LogFactory.getLog(LoggingTask.class);
    private static int count = 1;
    private Map<String, String> properties;

    private String stringToLog;

    @Override
    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
        this.stringToLog = properties.get(Constants.LOGGER_TASK_STRING_KEY);
    }

    @Override
    public void init() {

    }

    @Override
    public void execute() {
        log.error("Executing Task: " + stringToLog + ", Log Count: " + count + ", Thread Name: " +
                properties.get(Constants.THREAD_NAME_KEY));
        count += 1;
    }

}
