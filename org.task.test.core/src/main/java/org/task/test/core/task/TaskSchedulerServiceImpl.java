package org.task.test.core.task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.task.test.core.internal.TaskTestMgtDataHolder;
import org.task.test.core.util.Constants;
import org.wso2.carbon.ntask.common.TaskException;
import org.wso2.carbon.ntask.core.TaskInfo;
import org.wso2.carbon.ntask.core.TaskManager;
import org.wso2.carbon.ntask.core.service.TaskService;
import org.wso2.carbon.ntask.core.TaskInfo.TriggerInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TaskSchedulerServiceImpl {

    private static final Log log = LogFactory.getLog(TaskSchedulerServiceImpl.class);

    public TaskSchedulerServiceImpl() {
    }

    public void startTask(String stringToLog, long loggingFrequencyInSeconds) {
        if (loggingFrequencyInSeconds <= 0) {
            throw new RuntimeException("Time interval cannot be 0 or less than 0.");
        }
        long frequencyInMs = secondsToMilliseconds(loggingFrequencyInSeconds);
        try {
            TaskService taskService = TaskTestMgtDataHolder.getInstance().getTaskService();
            taskService.registerTaskType(Constants.LOGGER_TASK_TYPE);

            TaskManager taskManager = taskService.getTaskManager(Constants.LOGGER_TASK_TYPE);

            TriggerInfo triggerInfo = new TriggerInfo();
            triggerInfo.setIntervalMillis(frequencyInMs);
            triggerInfo.setRepeatCount(-1);

            Map<String, String> properties = new HashMap<>();
            String threadName = Thread.currentThread().getName();
            properties.put(Constants.THREAD_NAME_KEY, threadName);
            properties.put(Constants.LOGGER_TASK_STRING_KEY, stringToLog);

            String taskName = Constants.LOGGER_TASK_NAME;

            if (!taskManager.isTaskScheduled(taskName)) {

                TaskInfo taskInfo = new TaskInfo(taskName, Constants.LOGGER_TASK_CLAZZ,
                        properties, triggerInfo);

                taskManager.registerTask(taskInfo);
                taskManager.rescheduleTask(taskInfo.getName());
            } else {
                throw new RuntimeException("Logging task is already started");
            }


        } catch (TaskException e) {
            throw new RuntimeException("Error occurred while creating the logging task", e);
        }

    }

    public void stopTask() {
        try {
            String taskName = Constants.LOGGER_TASK_NAME;
            TaskService taskService = TaskTestMgtDataHolder.getInstance().getTaskService();
            if (taskService.isServerInit()) {
                TaskManager taskManager = taskService.getTaskManager(Constants.LOGGER_TASK_TYPE);
                taskManager.deleteTask(taskName);
            }
        } catch (TaskException e) {
            throw new RuntimeException("Error occurred while deleting the logging task", e);
        }
    }

    public List<TaskInfo> getAllTasks() throws TaskException {
        return TaskTestMgtDataHolder.getInstance().getTaskService().getTaskManager(Constants.LOGGER_TASK_TYPE).getAllTasks();
    }

    public void updateTask(String stringToLog, long frequencyInSeconds){
        long frequencyInMs = secondsToMilliseconds(frequencyInSeconds);
        try {
            String taskName = Constants.LOGGER_TASK_NAME;
            TaskService taskService = TaskTestMgtDataHolder.getInstance().getTaskService();
            TaskManager taskManager = taskService.getTaskManager(Constants.LOGGER_TASK_TYPE);

            if (taskManager.isTaskScheduled(taskName)) {
                taskManager.deleteTask(taskName);
                TriggerInfo triggerInfo = new TriggerInfo();
                triggerInfo.setIntervalMillis(frequencyInMs);
                triggerInfo.setRepeatCount(-1);

                Map<String, String> properties = new HashMap<>();
                String threadName = Thread.currentThread().getName();
                properties.put(Constants.THREAD_NAME_KEY, threadName);
                properties.put(Constants.LOGGER_TASK_STRING_KEY, stringToLog);

                TaskInfo taskInfo = new TaskInfo(taskName, Constants.LOGGER_TASK_CLAZZ, properties,
                        triggerInfo);
                taskManager.registerTask(taskInfo);
                taskManager.rescheduleTask(taskInfo.getName());
            } else {
                throw new RuntimeException("Logging task has not been started. Please start the task first.");
            }

        } catch (TaskException e) {
            throw new RuntimeException("Error occurred while updating the logging task", e);
        }

    }

    public boolean isTaskScheduled() {
        String taskName = Constants.LOGGER_TASK_NAME;
        TaskService taskService = TaskTestMgtDataHolder.getInstance().getTaskService();
        TaskManager taskManager;
        try {
            taskManager = taskService.getTaskManager(Constants.LOGGER_TASK_TYPE);
            return taskManager.isTaskScheduled(taskName);
        } catch (TaskException e) {
            throw new RuntimeException("Error occurred while checking if logging task is scheduled", e);
        }
    }

    private long secondsToMilliseconds(long seconds) {
        return TimeUnit.SECONDS.toMillis(seconds);
    }
}
