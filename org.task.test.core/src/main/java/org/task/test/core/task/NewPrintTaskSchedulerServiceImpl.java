package org.task.test.core.task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.task.test.core.internal.TaskTestMgtDataHolder;
import org.task.test.core.util.Constants;
import org.wso2.carbon.ntask.common.TaskException;
import org.wso2.carbon.ntask.core.TaskInfo;
import org.wso2.carbon.ntask.core.TaskInfo.TriggerInfo;
import org.wso2.carbon.ntask.core.TaskManager;
import org.wso2.carbon.ntask.core.service.TaskService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class NewPrintTaskSchedulerServiceImpl {

    private static final Log log = LogFactory.getLog(NewPrintTaskSchedulerServiceImpl.class);

    public NewPrintTaskSchedulerServiceImpl() {
    }

    public void startTaskInSeconds(String taskName, String stringToLog, long frequencyInSeconds) {
        long frequencyInMs = secondsToMilliseconds(frequencyInSeconds);
        startTask(taskName,stringToLog, frequencyInMs);
    }

    public void startTask(String taskName, String stringToLog, long frequencyInMs) {
        if (frequencyInMs <= 0) {
            throw new RuntimeException("Time interval cannot be 0 or less than 0.");
        }
        try {
            TaskService taskService = TaskTestMgtDataHolder.getInstance().getTaskService();
            taskService.registerTaskType(Constants.PRINT_TASK_TYPE);

            TaskManager taskManager = taskService.getTaskManager(Constants.PRINT_TASK_TYPE);

            TriggerInfo triggerInfo = new TriggerInfo();
            triggerInfo.setIntervalMillis(frequencyInMs);
            triggerInfo.setRepeatCount(-1);

            Map<String, String> properties = new HashMap<>();
            String threadName = Thread.currentThread().getName();
            properties.put(Constants.THREAD_NAME_KEY, threadName);
            properties.put(Constants.LOGGER_TASK_STRING_KEY, stringToLog);
            properties.put(Constants.NEW_PRINT_TASK_NAME_KEY, taskName);

            if (!taskManager.isTaskScheduled(taskName)) {

                TaskInfo taskInfo = new TaskInfo(taskName, Constants.PRINT_TASK_CLAZZ,
                        properties, triggerInfo);

                taskManager.registerTask(taskInfo);
                taskManager.rescheduleTask(taskInfo.getName());
            } else {
                throw new RuntimeException( "New task " + taskName + " is already started");
            }


        } catch (TaskException e) {
            throw new RuntimeException("Error occurred while creating the task: " + taskName, e);
        }

    }

    public void stopAllTasks() throws TaskException {
        List<TaskInfo> taskInfoList =  getAllTasks();
        for (TaskInfo info : taskInfoList) {
            stopTask(info.getName());
        }
    }

    public void stopTask(String taskName) {
        try {
            TaskService taskService = TaskTestMgtDataHolder.getInstance().getTaskService();
            if (taskService.isServerInit()) {
                TaskManager taskManager = taskService.getTaskManager(Constants.PRINT_TASK_TYPE);
                taskManager.deleteTask(taskName);
            }
        } catch (TaskException e) {
            throw new RuntimeException("Error occurred while deleting the task: " + taskName, e);
        }
    }

    public List<TaskInfo> getAllTasks() throws TaskException {
        return TaskTestMgtDataHolder.getInstance().getTaskService().getTaskManager(Constants.PRINT_TASK_TYPE).getAllTasks();
    }

    public void updateTask(String taskName, String stringToLog, long frequencyInSeconds){
        long frequencyInMs = secondsToMilliseconds(frequencyInSeconds);
        try {
            TaskService taskService = TaskTestMgtDataHolder.getInstance().getTaskService();
            TaskManager taskManager = taskService.getTaskManager(Constants.PRINT_TASK_TYPE);

            if (taskManager.isTaskScheduled(taskName)) {
                taskManager.deleteTask(taskName);
                TriggerInfo triggerInfo = new TriggerInfo();
                triggerInfo.setIntervalMillis(frequencyInMs);
                triggerInfo.setRepeatCount(-1);

                Map<String, String> properties = new HashMap<>();
                String threadName = Thread.currentThread().getName();
                properties.put(Constants.THREAD_NAME_KEY, threadName);
                properties.put(Constants.LOGGER_TASK_STRING_KEY, stringToLog);
                properties.put(Constants.NEW_PRINT_TASK_NAME_KEY, stringToLog);

                TaskInfo taskInfo = new TaskInfo(taskName, Constants.PRINT_TASK_CLAZZ, properties,
                        triggerInfo);
                taskManager.registerTask(taskInfo);
                taskManager.rescheduleTask(taskInfo.getName());
            } else {
                throw new RuntimeException(taskName + " task has not been started. Please start the task first.");
            }

        } catch (TaskException e) {
            throw new RuntimeException("Error occurred while updating the" + taskName +" task", e);
        }

    }

    public boolean isTaskScheduled(String taskName) {
        TaskService taskService = TaskTestMgtDataHolder.getInstance().getTaskService();
        TaskManager taskManager;
        try {
            taskManager = taskService.getTaskManager(Constants.PRINT_TASK_TYPE);
            return taskManager.isTaskScheduled(taskName);
        } catch (TaskException e) {
            throw new RuntimeException("Error occurred while checking if " + taskName + " task is scheduled.", e);
        }
    }

    private long secondsToMilliseconds(long seconds) {
        return TimeUnit.SECONDS.toMillis(seconds);
    }
    public void startAllTask(){
        try {
            List<TaskInfo> taskInfoList =  getAllTasks();
            for (TaskInfo info : taskInfoList) {
                startTask(info.getName(), info.getProperties().get(Constants.LOGGER_TASK_STRING_KEY), info.getTriggerInfo().getIntervalMillis());
            }
        }catch (TaskException e) {
            throw new RuntimeException("Error occurred while starting all the newly created tasks");
        }
    }
}
