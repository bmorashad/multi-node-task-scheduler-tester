package org.task.test.core.util;

public class Constants {
    public static String LOGGER_TASK_TYPE = "LOGGER_TASK";
    public static final String LOGGER_TASK_NAME = "LOGGING";
    public static final String LOGGER_TASK_STRING_KEY = "LOGGING_STRING";

    public static String PRINT_TASK_TYPE = "PRINT_TASK";
    public static String NEW_PRINT_TASK_NAME_KEY = "NEW_PRINT_TASK_NAME";

    public static String THREAD_NAME_KEY = "THREAD_NAME";
    public static String LOGGER_TASK_CLAZZ = "org.task.test.core.task.LoggingTask";
    public static String PRINT_TASK_CLAZZ = "org.task.test.core.task.PrintTask";
}
