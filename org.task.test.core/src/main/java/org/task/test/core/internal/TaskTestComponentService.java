package org.task.test.core.internal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import org.task.test.core.task.NewPrintTaskSchedulerServiceImpl;
import org.task.test.core.task.TaskSchedulerServiceImpl;
import org.wso2.carbon.ntask.core.service.TaskService;

/**
 * @scr.component name="org.task.test.task.manager" immediate="true"
 * @scr.reference name="ntask.component"
 * interface="org.wso2.carbon.ntask.core.service.TaskService"
 * cardinality="1..1"
 * policy="dynamic"
 * bind="setTaskService"
 * unbind="unsetTaskService"
 */
public class TaskTestComponentService {

    private static Log log = LogFactory.getLog(TaskTestComponentService.class);

    @SuppressWarnings("unused")
    protected void activate(ComponentContext componentContext) {

        BundleContext bundleContext = componentContext.getBundleContext();
        TaskSchedulerServiceImpl taskScheduleService = new TaskSchedulerServiceImpl();
        taskScheduleService.startTask("On Bundle Activate", 5);

        NewPrintTaskSchedulerServiceImpl newTaskSchedulerService = new NewPrintTaskSchedulerServiceImpl();
        newTaskSchedulerService.startAllTask();

    }

    @SuppressWarnings("unused")
    protected void deactivate(ComponentContext componentContext) {
        //do nothing
    }

    protected void setTaskService(TaskService taskService) {
        TaskTestMgtDataHolder.getInstance().setTaskService(taskService);
    }

    protected void unsetTaskService(TaskService taskService) {
    }



}
