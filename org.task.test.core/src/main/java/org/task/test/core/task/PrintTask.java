package org.task.test.core.task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.task.test.core.util.Constants;
import org.wso2.carbon.ntask.core.Task;

import java.util.Map;

public class PrintTask implements Task {

    private static final Log log = LogFactory.getLog(PrintTask.class);

    private String stringToLog;
    private String taskName;
    private Map<String, String> properties;

    @Override
    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
        this.stringToLog = properties.get(Constants.LOGGER_TASK_STRING_KEY);
        if (stringToLog == null) {
            this.stringToLog = "Default Sys Log";
        }
        taskName = properties.get(Constants.NEW_PRINT_TASK_NAME_KEY);
    }

    @Override
    public void init() {

    }

    @Override
    public void execute() {
        System.out.println("Task Name: " + taskName + ", New Task Log: " + stringToLog);
    }


}
