package org.task.test.core.internal;

import org.task.test.core.task.NewPrintTaskSchedulerServiceImpl;
import org.wso2.carbon.ntask.core.service.TaskService;

public class TaskTestMgtDataHolder {

    private TaskService taskService;
    private NewPrintTaskSchedulerServiceImpl newTaskSchedulerService;

    private TaskTestMgtDataHolder() {

    }

    public TaskService getTaskService() {
        return taskService;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public NewPrintTaskSchedulerServiceImpl getNewTaskSchedulerService() {
        return newTaskSchedulerService;
    }

    public void setNewTaskSchedulerService(NewPrintTaskSchedulerServiceImpl newTaskSchedulerService) {
        this.newTaskSchedulerService = newTaskSchedulerService;
    }

    public static class InstanceHolder {
        public static TaskTestMgtDataHolder instance = new TaskTestMgtDataHolder();
    }

    public static TaskTestMgtDataHolder getInstance() {
        return InstanceHolder.instance;
    }


}
